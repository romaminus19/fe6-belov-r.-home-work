/**
 * - змінити існуючу функцію createProduct і за допомогою
 * неї створити ще 3 функції: createIPhone, createSamsung, createXiaomi
 * - за допомогою створених функцій створити по 2 телефона кожного бренду
 * - до створюваних продуктів додати метод getProductName, 
 * який буде повертати повне ім'я продукту, наприклад iPhone 12 pro
 * - спробувати змінити brand у якомусь продукті
 * - вивести в консоль повне ім'я цього за допомогою методу getProductName
 * - додати метод buy, який буде приймати суму, якщо її достатньо для покупки, то
 * виводити в консоль 'Ви можете придбати BRAND MODEL', інакше 
 * 'Ви не можете придбати BRAND MODEL, вам не вистачає X грн'
 * - викликати метод buy з різними параметрами
 * 
 */

// function createProduct(brand, model, price) {
//     return { brand, model, price };
// }

class CreateProduct{
    constructor(brand, model, price){
        this.brand = brand;
        this.model = model;
        this.price = price;
    }
    getProductName = () => {
        return `${this.brand} ${this.model}`
    }
    buy = (value) => {
        if (this.price <= value) {
            console.log(`Ви можете придбати ${this.brand} ${this.model}`);
        } else {
            console.log(`Ви не можете придбати ${this.brand} ${this.model}`);
        }
    }
}

function createIPhone(){
    return new CreateProduct('iPhone', '12 pro', 27_000);
}
const iPhoneFirst = createIPhone();
const iPhoneSecond = createIPhone();

console.log(iPhoneFirst);
console.log(iPhoneSecond);
console.log(iPhoneFirst.getProductName());
console.log(iPhoneFirst.buy(3_000));
console.log(iPhoneFirst.buy(33_000));

function createSamsung(){
    return new CreateProduct('Samsung', 's10', 21_000);
}
const samsungFirst = createSamsung();
const samsungSecond = createSamsung();

function createXiaomi(){
    return new CreateProduct('Xiaomi', 'mi 11', 14_000);
}
const xiaomiFirst = createXiaomi();
const xiaomiSecond = createXiaomi();
