'use strict';

// class Person{
//     constructor(fName, lName){
//         this.fName = fName;
//         this.lName = lName;
        
//     }
//     connectWiFi(pass){
//         if (pass === '12345678') {
//             console.log('ok');
//         }else{
//             console.log('not ok');
//         }
//     }
//     comeToWorkSpace(){
//         console.log(`${this.fName} come To Work Space`);
//     }
//     makeCoffee(){
//         console.log(`${this.fName} made coffee`);
//     }
// }
// const mikePerson = new Person('Mike', 'Pitt', 'feOnline');
// const johnPerson = new Person('John', 'Dou', 'fe314');
// console.log(mikePerson);
// console.log(johnPerson);
// mikePerson.connectWiFi('12345678');

// class Student extends Person{
//     constructor(fName, lName, group){
//         super(fName, lName, group);
//         this.isAdvanced = group === 'fe314';
//         this.group = group;
//     }
// }

// const johnStudent = new Student('John', 'Dou', 'fe314');
// console.log(johnStudent);

// class Teacher extends Person{
//     constructor(fName, lName, salary){
//         super(fName, lName);
//         this.salary = salary;
//     }
//     startLesson(subj){
//         console.log(`${this.fName} has start ${subj}`);
//     }
// }

// const austinTeacher = new Teacher('Austin', 'Teach', 1000);
// console.log(austinTeacher);

// class Device{
//     constructor(company, model, price){
//         this.company = company;
//         this.model = model;
//         this.price = price;
//     }
//     getName(){
//         console.log(`${this.company} ${this.model}`);
//     }
//     getTotalPrice(){
//         return this.price * 1.2;
//     }
// }



// class Laptop extends Device{
//     constructor(company, model, price){
//         super(company, model, price);
//     }
//     getDeliveryPrice(){
//         if (this.getTotalPrice() > 30000) {
//             return 0;
//         }else{
//             return 200;
//         }
//     }
// }
// class Phone extends Device{
//     constructor(company, model, price, nfc = true){
//         super(company, model, price);
//         this.hasNfc = nfc;
//     }
//     getDeliveryPrice(){
//         if (this.getTotalPrice() > 30000) {
//             return this.getTotalPrice()*1.01;
//         }else{
//             return this.getTotalPrice()*1.03;
//         }
//     }
// }
// class Premium extends Phone{
//     constructor(company, model, price, nfc = true){
//         super(company, model, price, nfc);
//         this.hasNfc = nfc;
//     }
//     getDeliveryPrice(){
//         return this.price * 1.35;
//     }
// }
// const vertu = new Premium('vertu', 'gold', 50000)
// class Tablet extends Device{
//     constructor(company, model, price, sim = true){
//         super(company, model, price);
//         this.hasSim = sim;
//     }
//     getDeliveryPrice(){
//         if (this.getTotalPrice() > 10000) {
//             return ;
//         }else{
//             return this.getTotalPrice()*1.01;
//         }
//     }
// }

// const laptopHp007 = new Laptop('HP', 'n007', 12500);
// console.log(laptopHp007.getName());

// const iPhone10 = new Phone('apple', 'iPhone 10', 24000, );
// const xiaomiMi9t = new Phone('xiaomi', 'mi 9T', 14000, );
// const nokia3110 = new Phone('nokia', '3110', 300, false);
// console.log(iPhone10);
// console.log(xiaomiMi9t);
// console.log(nokia3110);

// const samsTablet = new Tablet('samsung', 'tablet', 21000, );
// console.log(samsTablet);
// console.log(samsTablet.getTotalPrice( ));

//================Class Work Task 2==========================

const logButton = document.querySelector('#login-btn');
const signButton = document.querySelector('#sign-up-btn');

class Modal {
    constructor(id, text, className) {
        this.id = id;
        this.text = text;
        this.className = className;
        this.el = null;
    }

    open = () => {
        this.el.classList.add('active');
    }

    close = () => {
        this.el.classList.remove('active');
    }

    render = () => {
        const body = document.querySelector('body');
        this.el = document.createElement('div');
        this.el.setAttribute('class', `${this.className}`);
        this.el.id = `${this.id}`;
        this.el.innerHTML = `<div class="modal-content">
                                <span class="close">X</span>
                                <h1>${this.text}</h1>
                            </div>`;
        this.el.querySelector('.close').addEventListener('click', this.close);
        body.append(this.el);
    }
}
const login = new Modal('login-modal', 'Ви успішно увійшли', 'modal login-modal');
const signUp = new Modal('sign-up-modal', 'Реєстрація', 'modal sign-up-modal');
login.render();
signUp.render();

logButton.addEventListener('click', login.open);
signButton.addEventListener('click', signUp.open);
