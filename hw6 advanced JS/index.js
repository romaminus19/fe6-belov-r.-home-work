const form = document.querySelector('.form');

form.addEventListener('submit', async (e)=>{
    e.preventDefault();
    const ip = await getIp('https://api.ipify.org/?format=json');
    const location = await getLocation(ip)
    renderLocation(location)
})

async function getIp(url) {
    const response = await fetch(url);
    const data = await response.json();
    return data;
}
async function getLocation({ip}) {
    const response = await fetch(`http://ip-api.com/json/${ip}?fields=status,message,continent,country,region,regionName,city,district`);
    const data = await response.json();
    return data;
}

function renderLocation({continent, country, region, regionName, city, district}) {
    form.insertAdjacentHTML('beforeend', `<ul class='ul'><li>Континент: <strong>${continent}</strong></li>
                                          <li>Країна: <strong>${country}</strong></li>
                                          <li>Регіон: <strong>${region}, ${regionName}</strong></li>
                                          <li>Місто: <strong>${city}</strong></li>
                                          <li>Район(міста): <strong>${district}</strong></li></ul>`)
}

