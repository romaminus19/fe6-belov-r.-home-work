'use strict'
//Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

// Коли важливо, аби програма не завершила роботу завчасно через дії, або не коректну 
// інф яка приходить в процесі роботи, а також коли дія для подальшого виконання 
// скрипту залежить, наприклад, від роботи сторонніх ресурсів

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];

const root = document.querySelector('#root');

function render(obj){
    root.innerHTML += `<li>name: ${obj.name} ---
                        author: ${obj.author} ---
                        price: ${obj.price}</el>`
};

let verificateArr = function(arr) {
    arr.forEach(el=>{
        try {
            if(!el.name){throw new Error(`Name is not found in ${el.name}`)};
            if(!el.author){throw new Error(`Author is not found in ${el.name}`)};
            if(!el.price){throw new Error(`Price is not found in ${el.name}`)};

            render(el);
        } catch (error) {
            console.log(error.message);
        }
    })
}
verificateArr(books)
