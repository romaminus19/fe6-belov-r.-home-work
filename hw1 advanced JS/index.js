'use strict';

//=================teoritical question================

//1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Це можливість мови розширювати наявні вже об'єкти

//2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Для того щоб викликати "батьківський" конструктор або методи того ж "батьківського" класу

//========================Task 1======================

class Employee{
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name(){
        return this._name;
    }
    set name(value){
        if(typeof value === 'string'){
            return this._name = value;
        }
    }
    get age(){
        return this._age;
    }
    set age(value){
        if(typeof value === 'number'){
            return this._age = value;
        }
    }
    get salary(){
        return this._salary;
    }
    set salary(value){
        if(typeof value === 'number'){
            return this._salary = value;
        }
    }
}
const someWho = new Employee('Some', 28, 30000);
console.log(someWho);
someWho.age = '15';
console.log(someWho);

class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang;
    }
    get salary(){
        return this._salary * 3;
    }
}
const programmerRoman = new Programmer("Roman", 28, 20000, 'js');
console.log(programmerRoman);
console.log(programmerRoman.salary);

const programmerNicole = new Programmer("Nicole", 25, 15000, 'js');
console.log(programmerNicole);
console.log(programmerNicole.salary);

const programmerJohn = new Programmer("John", 21, 17000, 'pyton');
console.log(programmerJohn);
console.log(programmerJohn.salary);









